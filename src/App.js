import React from 'react';
import { render } from 'react-dom';

const nombre = 'Miguel Angel';
const ConditionWelcome = ({idioma})=>{
  if(idioma === "es"){
    return <Saludo/>
  }
  else if(idioma === "en"){
    return <Greet/>
  }
}

const arrayNames = [
  'Miguel',
  'Carlos',
  'Juan',
  'Andres',
  'Messi'
]
function getArray(){
  const elementosLista = [];
  for(var i = 0; i < arrayNames.length; i++){
  elementosLista.push(<li>{arrayNames[i]}</li>);
  }
  return elementosLista;
}

const ArrayNames= () => {
return <ul>{ getArray()}</ul>
}

const MapNames= () => {
  return <ul>{
  arrayNames.map(nombre => <li>{nombre}</li>)
  }
  </ul>
  }

const Saludo = ()=>{  
return <p>Bienvenido mi nombre es {nombre}</p>
}

const Greet = ()=>{
return <p>Welcome my name is {nombre}</p>
}

const App = ()=>{
  return <h1><ConditionWelcome idioma="es"/> <ArrayNames/><MapNames/></h1>
}

render(
  <App/>, document.getElementById('root'),
  );
